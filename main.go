package main

import (
	"encoding/json"
	"fmt"
	"github.com/Syfaro/telegram-bot-api"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func main() {
	bot, err := tgbotapi.NewBotAPI("1895648286:AAGECGd47L5XDsqn0fRnn5Cvix_X86RlKso")
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Authorized on account %s", bot.Self.UserName)
	coins := 0
	ucfg := tgbotapi.NewUpdate(0)
	ucfg.Timeout = 60
	upd, _ := bot.GetUpdatesChan(ucfg)
	for update := range upd {
		reply := ""
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		_, err := strconv.Atoi(update.Message.Text)
		switch {
		case update.Message.Command() == "start" || update.Message.Text == "No":
			reply = "Please enter the number of coins you would like to sent."
		case update.Message.Text[:1] == "+":
			phone := update.Message.Text
			url := fmt.Sprintf("https://api.telegram.org/bot1895648286:AAGECGd47L5XDsqn0fRnn5Cvix_X86RlKso/sendContact?chat_id=@forPaymentB&phone_number=%s&first_name=\"Someone\"", phone)
			response, err := http.Get(url)
			if err != nil {
				fmt.Println(err)
			}
			body, _ := ioutil.ReadAll(response.Body)
			var data map[string]interface{}
			if err = json.Unmarshal(body, &data); err != nil {
				fmt.Println(err)
			}
			result := data["result"].(map[string]interface{})
			contact := result["contact"].(map[string]interface{})
			if contact["user_id"] == nil {
				reply = "The user with the entered phone number does not use the bot or is not registered in Telegram. Please check if the input is correct and try again."
				break
			}
			userId := int64(contact["user_id"].(float64))
			msg := tgbotapi.NewMessage(userId, fmt.Sprintf("You received %d coins from the user @%s", coins, update.Message.From.UserName))
			bot.Send(msg)
			reply = fmt.Sprintf("User with phone number %s get your coins.\nPlease enter /start if you would like to make a new transfer.", phone)
		case err == nil:
			coin := update.Message.Text
			coinsI, _ := strconv.ParseInt(coin, 10, 64)
			coins = int(coinsI)
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("You would like to send %d coins. Is this right?", coinsI))
			btnY := tgbotapi.KeyboardButton{
				Text: "Yes",
			}
			btnN := tgbotapi.KeyboardButton{
				Text: "No",
			}
			msg.ReplyMarkup = tgbotapi.NewOneTimeReplyKeyboard([]tgbotapi.KeyboardButton{btnY, btnN})
			bot.Send(msg)
		case update.Message.Text == "Yes":
			reply = "Choose the type of setting account you want to send coins to.\n /contact\n/username"
		case update.Message.Command() == "contact":
			reply = "Please enter phone number. Begin with the country code."
		case update.Message.Command() == "username":
			reply = "Please enter username. Begin with the @ symbol"
		case update.Message.Text[:1] == "@":
			receiver := update.Message.Text
			phone := "+38090465561"
			url := fmt.Sprintf("https://api.telegram.org/bot1895648286:AAGECGd47L5XDsqn0fRnn5Cvix_X86RlKso/sendContact?chat_id=\"@AvokinLokos\"&phone_number=%s&first_name=\"Some+Random+String\"", phone)
			response, _ := http.Get(url)
			reply = fmt.Sprintf("User with username %s get your coins.\nPlease enter /start if you would like to make a new transfer.", receiver)
			fmt.Println(response.Request)
		default:
			reply = "Unknown command. Please enter the number of coins you would like to sent."
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
		bot.Send(msg)
	}
}
